// vote model
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var VoteSchema = new Schema({
  selection: String
});

mongoose.model('Vote', VoteSchema);

