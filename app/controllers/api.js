var express = require('express'),
    router = express.Router(),
    async = require('async'),
    mongoose = require('mongoose'),
    Vote = mongoose.model('Vote');

router.get('/', function (req, res) {
  res.json({ ping: Date.now() });
});


//REDIS?
//ERROR HANDLING

//get results

//save vote

//return results


router.post('/vote', function(req, res) {


    if ( req.body.selection !== 'batman' && req.body.selection !== 'superman' ) {
      res.json({ error: 'bad selection' });
      return;
    }


    async.waterfall([
      function saveVote(callback) {
          var vote = new Vote(req.body);

          vote.save(function(err) {
            if (err) {}

            console.log('saved data');
            callback(null);

          });


      },
      function countResults(callback) {
          var counter = 0;

          Vote.find(function(err, votes) {
            if (err) {}

              //count votes
            for (var i = 0; i < votes.length; i++) {
              if (votes[i].selection === req.body.selection) counter++;
            }

            console.log('fetched and counted results');

            callback(null, Math.round(counter / votes.length * 100));

          });
      }],

      function(err, results) {

        res.json({
          error: err,
          result: results
        });
      }
    );

});


//export app router
module.exports = function (app) {
  app.use('/api', router);
};
