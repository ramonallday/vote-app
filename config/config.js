var path = require('path'),
    rootPath = path.normalize(__dirname + '/..'),
    port = 3009,
    env = process.env.NODE_ENV || 'development';

var config = {
  development: {
    root: rootPath,
    app: {
      name: 'batmanapp'
    },
    port: port,
    db: 'mongodb://localhost/batmanapp-development'
  },

  test: {
    root: rootPath,
    app: {
      name: 'batmanapp'
    },
    port: port,
    db: 'mongodb://localhost/batmanapp-test'
  },

  production: {
    root: rootPath,
    app: {
      name: 'batmanapp'
    },
    port: port,
    db: 'mongodb://localhost/batmanapp-production'
  }
};

module.exports = config[env];
